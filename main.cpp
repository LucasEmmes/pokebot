#include <iostream>
#include "restclient-cpp/restclient.h"
#include <nlohmann/json.hpp>
using json = nlohmann::json;

// Check if pokemon appears in pokedex
bool caught_before(const json& pokemon, const json& pokedex) {
    for (int i = 0; i < pokedex.size(); i++)
    {
        std::string entry_data = pokedex[i]["data"];
        json pokedex_entry = json::parse(entry_data);
        if (pokedex_entry["name"] == pokemon["name"]) return true;
    }
    return false;
}

int main(int argc, char* argv[]) {
    
    // Generate random number
    srand(time(0));
    int random_id = (rand()%898)+1;

    // Query the API and parse json
    RestClient::Response pokemon_api_response = RestClient::get("https://pokeapi.co/api/v2/pokemon/" + std::to_string(random_id));
    json full_pokemon_json =json::parse(pokemon_api_response.body);

    // "Catch"
    json caught_pokemon;
    caught_pokemon["name"] = full_pokemon_json["name"];
    caught_pokemon["sprite"] = full_pokemon_json["sprites"]["front_default"];
    for (int i = 0; i < full_pokemon_json["types"].size(); i++)
        caught_pokemon["types"].push_back(full_pokemon_json["types"][i]["type"]["name"]);

    std::cout << "Found a " << caught_pokemon["name"] << std::endl;

    // Check if we've caught it before
    RestClient::Response get_pokemon_response = RestClient::get("127.0.0.1:5000/fetch");
    json pokedex = json::parse(get_pokemon_response.body);
    if (caught_before(caught_pokemon, pokedex)) {
        std::cout << "We have already caught " << caught_pokemon["name"] << std::endl;
        return 0;
    }

    // Upload to database
    json packet;
    packet["meta"] = "pokemon";
    packet["data"] = caught_pokemon.dump();
    std::cout << "Registering " << caught_pokemon["name"] << " in the database" << std::endl;
    RestClient::Response database_response = RestClient::post("127.0.0.1:5000/submit", "application/json", packet.dump());
    std::cout << database_response.code << std::endl;

    return 0;
}
