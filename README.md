# PokeBot
PokeBot is a program that will generate a random number,  
query the pokeAPI, and catch the pokemon!  

It uses REST in CPP to query, and python for its database.

## Contributors
Christoffer Mundbjerg-Sunne
Lucas Emmes